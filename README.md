Author: Wil Sprouse, wils@cs.uoregon.edu

Description: This is simple Anagram game that runs on a flask webserver, which implements AJAX
	     and JQuery functionalities so a user submit their answer without pressing a button.
	     Written for CIS 322, Spring 2020
